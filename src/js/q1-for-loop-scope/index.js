/* eslint-disable */

/**
 * # Question 1 - For Loop Scope
 *
 * Given the code below, show at least two ways that you could get the correct
 * output: 0, 1, 2, 3, 4
 */

/**
 * Solution 1
 */
(() => {
  for (var i = 0; i < 5; i = 5 * i++) {
    setTimeout(function () {
      console.log('solution1: ' + i/5);
    }, i * 500);
  }
})();

/**
 * Solution 2
 */
(() => {
  for (var i = 10; i < 15; ++i) {
    setTimeout(function () {
      console.log('solution2: ' + i%10);
    }, i * 500);
  }
})();

/**
 * Solution 3
 */
(() => {
  for (var i = 0; i < 500; i = i + 100) {
    setTimeout(function () {
      console.log('solution3: ' + i / 100);
    }, i * 500);
  }
})();
