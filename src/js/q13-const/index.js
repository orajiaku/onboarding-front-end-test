/* eslint-disable */

/**
 * # Question 13 - Const
 *
 */

const data = {
  name: "Mc Jagger"
};

data.name = "Mike Tyson";

// 13a - Why does it allow us to change `name` even though `data` is a const ?

// ----------------------------------------------

data = {
  name: "Michelle Obama"
};

// 13b - What will happen here? Why?
/*
  WRITE YOUR ANSWER HERE
  ----------------------
  ## Answer 13a
 It would allow you to change it because you arent changing the original variable "data" that was declared as a const

  ## Answer 13b
 This would five an error because here you are trying to re-assign a const variable

*/
