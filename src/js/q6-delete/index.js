/**
 * # Question 6:
 *
 * 1. What is the length of the Array?
 * 2. Why?
 * 3. What would be a better way to remove an item from an Array?
 */

const arr = [1, 2, 3];
delete arr[1];

/*
  WRITE YOUR ANSWER HERE
  ----------------------
  ## Answer 1
  The length is 3

  ## Answer 2
  Delete in javascript just removes the item but doesn't collapse the array

  ## Answer 3
  A better way is to splice the array
*/
