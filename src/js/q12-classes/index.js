/**
 * # Question 12 - Classes
 *
 * Convert the following constructor function to a class
 */

const Person = function(first, last) {
  this.first = first;
  this.last = last;

  this.getName = function () {
    return this.first + ' ' + this.last;
  };
};
